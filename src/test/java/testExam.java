import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class testExam {
	
	private int n;
	private boolean exp1;
	
	private int a,b;
	private boolean exp2;
	
	private int n2, exp3;
	
	@Parameters
	public static Collection<Object[]> numbers(){
		return Arrays.asList(new Object[][] {
			{0,false,3,1,true,4,2},
			{6,false,5,2,false,10,5},
			{10,false,10,5,true,3,1},
			{5,true,120,40,true,78,39},
			{1,false,119,5,false,24,12}
		});
	}
	
	public testExam(int n, boolean exp1, int a, int b, boolean exp2, int n2, int exp3) {
		this.n=n;
		this.exp1=exp1;
		this.a=a;
		this.b=b;
		this.exp2=exp2;
		this.n2=n2;
		this.exp3=exp3;
	}
	
	@Test
	public void testExamFunction() {
		assertEquals(exp1, exam1.exam(n));
	}
	@Test
	public void testGetRestZero() {
		assertEquals(exp2, exam1.getRestZero(a, b));
	}
	@Test
	public void testOneHalf() {
		assertEquals(exp3, exam1.oneHalf(n2));
	}

}
